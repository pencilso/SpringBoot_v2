package com.fc.test.common.quartz;

import com.fc.test.common.quartz.utils.JobInvokeUtil;
import com.fc.test.dao.model.SysQuartzJobModel;
import org.quartz.JobExecutionContext;

/**
 * 定时任务处理（允许并发执行）
 * 
 * @author  jan  橙寂
 *
 */
public class QuartzJobExecution extends AbstractQuartzJob
{
    @Override
    protected void doExecute(JobExecutionContext context, SysQuartzJobModel sysJob) throws Exception {
        JobInvokeUtil.invokeMethod(sysJob);
    }


}
