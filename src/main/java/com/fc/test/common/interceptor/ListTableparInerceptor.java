package com.fc.test.common.interceptor;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * @author pencilso
 * @date 2020/4/22 4:32 下午
 */
public interface ListTableparInerceptor<M> {
    void call(QueryWrapper<M> queryWrapper);
}
