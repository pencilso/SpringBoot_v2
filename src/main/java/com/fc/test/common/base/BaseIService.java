package com.fc.test.common.base;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fc.test.common.interceptor.ListTableparInerceptor;
import com.fc.test.model.custom.Tablepar;
import com.fc.test.util.StringUtils;

import java.util.List;

/**
 * @author pencilso
 * @date 2020/4/22 2:57 下午
 */
public interface BaseIService<M> extends IService<M> {


    default <T> PageInfo<T> iPageToPageInfo(Page<T> page) {
        return new PageInfo<T>()
                .setList(page.getRecords())
                .setPageNum((int) page.getCurrent())
                .setPages((int) page.getPages())
                .setPageSize((int) page.getSize())
                .setTotal(page.getTotal())
                .setSize(page.getRecords().size());
    }

    default <T> PageInfo<M> listTablepar(Tablepar tablepar, String column, String searchText) {
        return listTablepar(tablepar, column, searchText, null);
    }

    /**
     * 默认分页
     *
     * @param tablepar
     * @param column
     * @param searchText
     * @param <T>
     * @return
     */
    default <T> PageInfo<M> listTablepar(Tablepar tablepar, String column, String searchText, ListTableparInerceptor<M> listTableparInerceptor) {
        QueryWrapper<M> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("id");
        if (StringUtils.isNotBlank(searchText)) {
            queryWrapper.like(column, searchText);
        }

        if (listTableparInerceptor != null) listTableparInerceptor.call(queryWrapper);

        if (StringUtils.isNotBlank(tablepar.getOrderByColumn())) {
            String underColumn = StringUtils.toUnderScoreCase(tablepar.getOrderByColumn());
            queryWrapper.orderBy(true, "asc".equals(tablepar.getIsAsc()), underColumn);
        }
        Page<M> page = page(new Page<>(tablepar.getPageNum(), tablepar.getPageSize()), queryWrapper);
        return iPageToPageInfo(page);
    }

    /**
     * 检查名字
     *
     * @param column
     * @param name
     * @return
     */
    default int checkNameUnique(String column, String name) {
        QueryWrapper<M> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(column, name);
        List<M> list = list(queryWrapper);
        return list.size();
    }
}
