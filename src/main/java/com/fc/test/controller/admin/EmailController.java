package com.fc.test.controller.admin;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.fc.test.common.base.PageInfo;
import com.fc.test.dao.model.SysEmailModel;
import com.fc.test.dao.service.SysEmailService;
import com.fc.test.model.custom.TableSplitResult;
import com.fc.test.util.StringUtils;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fc.test.common.base.BaseController;
import com.fc.test.common.domain.AjaxResult;
import com.fc.test.model.custom.Tablepar;
import com.fc.test.model.custom.TitleVo;
import com.fc.test.util.SimpleEmailUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 邮件发送Controller
 *
 * @author fuce
 * @version V1.0
 * @ClassName: EmailController
 * @date 2019-06-10 00:39
 */
@Api(value = "邮件发送Controller")
@Controller
@RequestMapping("/EmailController")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class EmailController extends BaseController {

    private String prefix = "admin/email";

    private final SysEmailService sysEmailService;

    /**
     * 分页展示页面
     *
     * @param model
     * @param model
     * @return
     */
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/view")
    @RequiresPermissions("system:email:view")
    public String view(ModelMap model) {
        String str = "邮件";
        setTitle(model, new TitleVo("发送", str + "管理", true, "欢迎进入" + str + "页面", true, false));
        return prefix + "/list";
    }

    /**
     * 分页list页面
     *
     * @param tablepar
     * @param searchText
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @PostMapping("/list")
    @RequiresPermissions("system:email:list")
    @ResponseBody
    public Object list(Tablepar tablepar, String searchText) {
        PageInfo<SysEmailModel> page = sysEmailService.listTablepar(tablepar, "content", searchText);
        TableSplitResult<SysEmailModel> result = new TableSplitResult<SysEmailModel>(page.getPageNum(), page.getTotal(), page.getList());
        return result;
    }

    /**
     * 新增跳转
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap) {
        return prefix + "/add";
    }

    /**
     * 新增
     *
     * @param tSysEmail
     * @param model
     * @return
     * @throws Exception
     * @author fuce
     */
    //@Log(title = "新增邮件", action = "1")
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    @RequiresPermissions("system:email:add")
    @ResponseBody
    public AjaxResult add(SysEmailModel tSysEmail, Model model) throws Exception {
        return toAjax(sysEmailService.save(tSysEmail));
    }

    /**
     * 删除邮件
     *
     * @param ids
     * @return
     */
    //@Log(title = "删除邮件", action = "1")
    @ApiOperation(value = "删除", notes = "删除")
    @PostMapping("/remove")
    @RequiresPermissions("system:email:remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(sysEmailService.removeByIds(StringUtils.splitList(ids, StringPool.COMMA)));
    }

    /**
     * 检查邮件同名
     *
     * @param tSysEmail
     * @return
     */
    @ApiOperation(value = "检查Name唯一", notes = "检查Name唯一")
    @PostMapping("/checkNameUnique")
    @ResponseBody
    public int checkNameUnique(SysEmailModel tSysEmail) {
        int b = sysEmailService.checkNameUnique("content", tSysEmail.getContent());
        return b > 0 ? 1 : 0;
    }


    /**
     * 修改跳转
     *
     * @param id
     * @param mmap
     * @return
     */
    @ApiOperation(value = "修改跳转", notes = "修改跳转")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {
        mmap.put("TSysEmail", sysEmailService.getById(id));

        return prefix + "/edit";
    }

    /**
     * 修改保存
     */
    //@Log(title = "修改保存", action = "1")
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("system:email:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysEmailModel record) {
        return toAjax(sysEmailService.updateById(record));
    }

}
