package com.fc.test.controller.admin;

import javax.servlet.http.HttpServletRequest;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.fc.test.common.base.BaseController;
import com.fc.test.common.base.PageInfo;
import com.fc.test.common.domain.AjaxResult;
import com.fc.test.dao.model.SysRoleModel;
import com.fc.test.model.custom.BootstrapTree;
import com.fc.test.model.custom.TableSplitResult;
import com.fc.test.model.custom.Tablepar;
import com.fc.test.model.custom.TitleVo;
import com.fc.test.shiro.util.ShiroUtils;
import com.fc.test.util.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 角色Controller
 *
 * @author fuce
 * @date: 2018年9月2日 下午8:08:21
 */
@Api(value = "用户角色")
@Controller
@RequestMapping("/RoleController")
public class RoleController extends BaseController {


    //跳转页面参数
    private String prefix = "admin/role";

    /**
     * 展示页面
     *
     * @param model
     * @return
     * @author fuce
     * @Date 2019年11月11日 下午4:01:58
     */
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/view")
    @RequiresPermissions("system:role:view")
    public String view(ModelMap model) {
        String str = "角色";
        setTitle(model, new TitleVo("列表", str + "管理", true, "欢迎进入" + str + "页面", true, false));
        return prefix + "/list";
    }

    /**
     * 角色列表
     *
     * @param tablepar
     * @param searchText 搜索字符
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @PostMapping("/list")
    @RequiresPermissions("system:user:list")
    @ResponseBody
    public Object list(Tablepar tablepar, String searchText) {
        PageInfo<SysRoleModel> page = sysRoleService.listTablepar(tablepar, "name", searchText);
        return new TableSplitResult<SysRoleModel>(page.getPageNum(), page.getTotal(), page.getList());
    }

    /**
     * 新增角色
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }


    /**
     * 角色添加
     *
     * @param role
     * @return
     */
    //@Log(title = "角色添加", action = "1")
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    @RequiresPermissions("system:user:add")
    @ResponseBody
    public AjaxResult add(SysRoleModel role, String prem) {
        return toAjax(sysRoleService.insertRoleAndPrem(role, prem));
    }

    /**
     * 删除角色
     *
     * @param ids
     * @return
     */
    //@Log(title = "删除角色", action = "1")
    @ApiOperation(value = "删除", notes = "删除")
    @PostMapping("/remove")
    @RequiresPermissions("system:user:remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(sysRoleService.removeByIds(StringUtils.splitList(ids, StringPool.COMMA)));
    }

    /**
     * 检查角色
     *
     * @param tsysRole
     * @return
     */
    @ApiOperation(value = "检查Name唯一", notes = "检查Name唯一")
    @PostMapping("/checkNameUnique")
    @ResponseBody
    public int checkNameUnique(SysRoleModel tsysRole) {
        int b = sysRoleService.checkNameUnique("name", tsysRole.getName());
        return b > 0 ? 1 : 0;
    }


    /**
     * 修改角色
     *
     * @param id
     * @param mmap
     * @return
     */
    @ApiOperation(value = "修改跳转", notes = "修改跳转")
    @GetMapping("/edit/{roleId}")
    public String edit(@PathVariable("roleId") String id, ModelMap mmap) {
        mmap.put("TsysRole", sysRoleService.getById(id));
        return prefix + "/edit";
    }

    /**
     * 修改保存角色
     */
    //@Log(title = "修改保存角色", action = "1")
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("system:user:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysRoleModel tsysRole, String prem, HttpServletRequest request) {
        boolean updateRoleAndPrem = sysRoleService.updateRoleAndPrem(tsysRole, prem);
        if (updateRoleAndPrem) {
            //大于0刷新权限
            ShiroUtils.clearCachedAuthorizationInfo();
            //获取菜单栏
            BootstrapTree bootstrapTree = sysPermissionService.getbooBootstrapTreePerm(ShiroUtils.getUserId());
            request.getSession().setAttribute("bootstrapTree", bootstrapTree);
        }
        return toAjax(updateRoleAndPrem);
    }
}
