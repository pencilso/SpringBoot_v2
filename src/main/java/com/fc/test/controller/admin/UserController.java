package com.fc.test.controller.admin;

import java.util.List;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.fc.test.common.base.PageInfo;
import com.fc.test.dao.model.SysRoleModel;
import com.fc.test.dao.model.SysUserModel;
import com.fc.test.model.custom.TableSplitResult;
import com.fc.test.util.StringUtils;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fc.test.common.base.BaseController;
import com.fc.test.common.domain.AjaxResult;
import com.fc.test.common.log.Log;
import com.fc.test.model.custom.RoleVo;
import com.fc.test.model.custom.Tablepar;
import com.fc.test.model.custom.TitleVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 用户Controller
 *
 * @author fuce
 * @ClassName: UserController
 * @date 2019-11-20 22:35
 */
@Api(value = "用户数据")
@Controller
@RequestMapping("/UserController")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserController extends BaseController {

    private String prefix = "admin/user";

    /**
     * 展示跳转页面
     *
     * @param model
     * @return
     * @author fuce
     * @Date 2019年11月11日 下午4:14:34
     */
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/view")
    @RequiresPermissions("system:user:view")
    public String view(ModelMap model) {
        String str = "用户";
        setTitle(model, new TitleVo("列表", str + "管理", true, "欢迎进入" + str + "页面", true, false));
        return prefix + "/list";
    }


    /**
     * list集合
     *
     * @param tablepar
     * @param searchText
     * @return
     * @author fuce
     * @Date 2019年11月11日 下午4:14:40
     */
    //@Log(title = "分页查询", action = "1")
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @PostMapping("/list")
    @RequiresPermissions("system:user:list")
    @ResponseBody
    public Object list(Tablepar tablepar, String searchText) {
        PageInfo<SysUserModel> page = sysUserService.listTablepar(tablepar, "username", searchText);
        return new TableSplitResult<SysUserModel>(page.getPageNum(), page.getTotal(), page.getList());
    }

    /**
     * 新增跳转
     *
     * @param modelMap
     * @return
     * @author fuce
     * @Date 2019年11月11日 下午4:14:51
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap) {
        //添加角色列表
        List<SysRoleModel> tsysRoleList = sysRoleService.list();
        modelMap.put("tsysRoleList", tsysRoleList);
        return prefix + "/add";
    }

    /**
     * 新增保存
     *
     * @param user
     * @param model
     * @param roles
     * @return
     * @author fuce
     * @Date 2019年11月11日 下午4:14:57
     */
    @Log(title = "用户新增", action = "111")
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    @RequiresPermissions("system:user:add")
    @ResponseBody
    public AjaxResult add(SysUserModel user, Model model, @RequestParam(value = "roles", required = false) List<String> roles) {
        return toAjax(sysUserService.insertUserRoles(user, roles));
    }

    /**
     * 删除用户
     *
     * @param ids
     * @return
     */
    //@Log(title = "删除用户", action = "1")
    @ApiOperation(value = "删除", notes = "删除")
    @PostMapping("/remove")
    @RequiresPermissions("system:user:remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(sysUserService.removeByIds(StringUtils.splitList(ids, StringPool.COMMA)));
    }

    /**
     * 检查用户
     *
     * @param sysUserModel
     * @return
     */
    @ApiOperation(value = "检查Name唯一", notes = "检查Name唯一")
    @PostMapping("/checkLoginNameUnique")
    @ResponseBody
    public int checkLoginNameUnique(SysUserModel sysUserModel) {
        int b = sysUserService.checkNameUnique("username", sysUserModel.getUsername());
        return b > 0 ? 1 : 0;
    }


    /**
     * 修改用户跳转
     *
     * @param id
     * @param mmap
     * @return
     */
    @ApiOperation(value = "修改跳转", notes = "修改跳转")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {
        //查询所有角色
        List<RoleVo> roleVos = sysUserService.getUserIsRole(id);
        mmap.put("roleVos", roleVos);
        mmap.put("TsysUser", sysUserService.getById(id));

        return prefix + "/edit";
    }

    /**
     * 修改保存用户
     */
    //@Log(title = "修改保存用户", action = "1")
    @ApiOperation(value = "修改保存用户", notes = "修改保存用户")
    @RequiresPermissions("system:user:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysUserModel tsysUser, @RequestParam(value = "roles", required = false) List<String> roles) {
        return toAjax(sysUserService.updateUserRoles(tsysUser, roles));
    }


    /**
     * 修改用户密码跳转
     *
     * @param id
     * @param mmap
     * @return
     */
    //@Log(title = "修改用户密码", action = "1")
    @ApiOperation(value = "修改用户密码跳转", notes = "修改用户密码跳转")
    @GetMapping("/editPwd/{id}")
    public String editPwd(@PathVariable("id") String id, ModelMap mmap) {
        mmap.put("TsysUser", sysUserService.getById(id));
        return prefix + "/editPwd";
    }

    /**
     * 修改保存用户
     */
    //@Log(title = "修改用户密码", action = "1")
    @ApiOperation(value = "修改用户密码", notes = "修改用户密码")
    @RequiresPermissions("system:user:editPwd")
    @PostMapping("/editPwd")
    @ResponseBody
    public AjaxResult editPwdSave(SysUserModel tsysUser) {
        return toAjax(sysUserService.updateById(tsysUser));
    }


}
