package com.fc.test.controller.admin;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.fc.test.common.base.PageInfo;
import com.fc.test.dao.model.SysCityModel;
import com.fc.test.dao.service.SysCityService;
import com.fc.test.util.StringUtils;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fc.test.common.base.BaseController;
import com.fc.test.common.domain.AjaxResult;
import com.fc.test.model.custom.TableSplitResult;
import com.fc.test.model.custom.Tablepar;
import com.fc.test.model.custom.TitleVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 城市Controller
 *
 * @author fuce
 * @ClassName: SysCityController
 * @date 2019-11-20 22:31
 */
@Api(value = "城市设置")
@Controller
@RequestMapping("/SysCityController")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SysCityController extends BaseController {

    private String prefix = "admin/province/sysCity";
    private final SysCityService sysCityService;

    /**
     * 城市设置展示跳转
     *
     * @param model
     * @return
     */
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/view")
    @RequiresPermissions("gen:sysCity:view")
    public String view(ModelMap model) {
        String str = "城市设置";
        setTitle(model, new TitleVo("列表", str + "管理", true, "欢迎进入" + str + "页面", true, false));
        return prefix + "/list";
    }

    /**
     * 城市设置list
     *
     * @param tablepar
     * @param searchText
     * @return
     */
    //@Log(title = "城市设置集合查询", action = "111")
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @PostMapping("/list")
    @RequiresPermissions("gen:sysCity:list")
    @ResponseBody
    public Object list(Tablepar tablepar, String searchText) {
        PageInfo<SysCityModel> page = sysCityService.listTablepar(tablepar, "city_name", searchText);
        TableSplitResult<SysCityModel> result = new TableSplitResult<SysCityModel>(page.getPageNum(), page.getTotal(), page.getList());
        return result;
    }

    /**
     * 新增跳转
     *
     * @param modelMap
     * @return
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap) {
        return prefix + "/add";
    }

    /**
     * 新增保存
     *
     * @param sysCity
     * @return
     */
    //@Log(title = "城市设置新增", action = "111")
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    @RequiresPermissions("gen:sysCity:add")
    @ResponseBody
    public AjaxResult add(SysCityModel sysCity) {
        return toAjax(sysCityService.save(sysCity));
    }

    /**
     * 删除
     *
     * @param ids
     * @return
     */
    //@Log(title = "城市设置删除", action = "111")
    @ApiOperation(value = "删除", notes = "删除")
    @PostMapping("/remove")
    @RequiresPermissions("gen:sysCity:remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(sysCityService.removeByIds(StringUtils.splitList(ids, StringPool.COMMA)));
    }

    /**
     * 检查
     *
     * @param tsysUser
     * @return
     */
    @ApiOperation(value = "检查Name唯一", notes = "检查Name唯一")
    @PostMapping("/checkNameUnique")
    @ResponseBody
    public int checkNameUnique(SysCityModel sysCity) {
        int b = sysCityService.checkNameUnique("city_name",sysCity.getCityName());
        return b > 0 ? 1 : 0;
    }


    /**
     * 修改跳转
     *
     * @param id
     * @param mmap
     * @return
     */
    @ApiOperation(value = "修改跳转", notes = "修改跳转")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, ModelMap mmap) {
        mmap.put("SysCity", sysCityService.getById(id));

        return prefix + "/edit";
    }

    /**
     * 修改保存
     */
    //@Log(title = "城市设置修改", action = "111")
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:sysCity:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysCityModel record) {
        return toAjax(sysCityService.updateById(record));
    }


}
