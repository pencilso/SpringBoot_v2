package com.fc.test.controller.admin;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.fc.test.common.base.BaseController;
import com.fc.test.common.base.PageInfo;
import com.fc.test.common.domain.AjaxResult;
import com.fc.test.dao.model.SysPositionModel;
import com.fc.test.dao.service.SysPositionService;
import com.fc.test.model.custom.TableSplitResult;
import com.fc.test.model.custom.Tablepar;
import com.fc.test.model.custom.TitleVo;
import com.fc.test.util.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Controller
@RequestMapping("/SysPositionController")
@Api(value = "岗位表")
public class SysPositionController extends BaseController {
    private final SysPositionService sysPositionService;

    private String prefix = "admin/sysPosition";

    /**
     * list展示
     */
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/view")
    @RequiresPermissions("gen:sysPosition:view")
    public String view(ModelMap model) {
        String str = "岗位表";
        setTitle(model, new TitleVo("列表", str + "管理", true, "欢迎进入" + str + "页面", true, false));
        return prefix + "/list";
    }

    /**
     * 分页集合
     */
    //@Log(title = "岗位表集合查询", action = "111")
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @PostMapping("/list")
    @RequiresPermissions("gen:sysPosition:list")
    @ResponseBody
    public Object list(Tablepar tablepar, String searchText) {
        PageInfo<SysPositionModel> sysPositionModelPageInfo = sysPositionService.listTablepar(tablepar, "post_name", searchText);
        TableSplitResult<SysPositionModel> result = new TableSplitResult<SysPositionModel>(sysPositionModelPageInfo.getPageNum(), sysPositionModelPageInfo.getTotal(), sysPositionModelPageInfo.getList());
        return result;
    }

    /**
     * 新增跳转
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap) {
        return prefix + "/add";
    }

    /**
     * 新增
     */
    //@Log(title = "岗位表新增", action = "111")
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("add")
    @RequiresPermissions("gen:sysPosition:add")
    @ResponseBody
    public AjaxResult add(SysPositionModel sysPosition) {
        return toAjax(sysPositionService.save(sysPosition));
    }

    /**
     * 删除用户
     *
     * @param ids
     * @return
     */
    //@Log(title = "岗位表删除", action = "111")
    @ApiOperation(value = "删除", notes = "删除")
    @PostMapping("/remove")
    @RequiresPermissions("gen:sysPosition:remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(sysPositionService.removeByIds(StringUtils.splitList(ids, StringPool.COMMA)));
    }

    /**
     * 检查用户
     *
     * @return
     */
    @ApiOperation(value = "检查Name唯一", notes = "检查Name唯一")
    @PostMapping("/checkNameUnique")
    @ResponseBody
    public int checkNameUnique(SysPositionModel sysPosition) {
        return sysPositionService.checkNameUnique("post_name", sysPosition.getPostName()) > 0 ? 1 : 0;
    }


    /**
     * 修改跳转
     *
     * @param id
     * @param mmap
     * @return
     */
    @ApiOperation(value = "修改跳转", notes = "修改跳转")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {
        mmap.put("SysPosition", sysPositionService.getById(id));
        return prefix + "/edit";
    }

    /**
     * 修改保存
     */
    //@Log(title = "岗位表修改", action = "111")
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:sysPosition:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysPositionModel record) {
        return toAjax(sysPositionService.updateById(record));
    }


    /**
     * 根据主键查询
     *
     * @param id
     * @return
     */
    @PostMapping("/get/{id}")
    @ApiOperation(value = "根据id查询唯一", notes = "根据id查询唯一")
    public SysPositionModel edit(@PathVariable("id") String id) {
        return sysPositionService.getById(id);
    }


}
