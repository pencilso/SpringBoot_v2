package com.fc.test.dao.model;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 文件表存储表
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_sys_datas")
public class SysDatasModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    /**
     * 文件地址
     */
    private String filePath;

    /**
     * 后缀
     */
    private String fileSuffix;


}
