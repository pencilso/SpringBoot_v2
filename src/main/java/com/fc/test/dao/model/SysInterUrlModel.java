package com.fc.test.dao.model;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 拦截url表
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_sys_inter_url")
public class SysInterUrlModel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 拦截名称
     */
    private String interName;

    /**
     * 拦截url
     */
    private String url;

    /**
     * 类型
     */
    private Integer type;


}
