package com.fc.test.dao.model;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 日志记录表
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_sys_oper_log")
public class SysOperLogModel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 标题
     */
    private String title;

    /**
     * 方法
     */
    private String method;

    /**
     * 操作人
     */
    private String operName;

    /**
     * url
     */
    private String operUrl;

    /**
     * 参数
     */
    private String operParam;

    private String errorMsg;

    /**
     * 操作时间
     */
    private Date operTime;


}
