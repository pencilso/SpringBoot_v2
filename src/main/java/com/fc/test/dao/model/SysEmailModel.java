package com.fc.test.dao.model;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 电子邮件
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_sys_email")
public class SysEmailModel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 接收人电子邮件
     */
    private String receiversEmail;

    /**
     * 邮件标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * 发送人id
     */
    private String sendUserId;

    /**
     * 发送人账号
     */
    private String sendUserName;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;


}
