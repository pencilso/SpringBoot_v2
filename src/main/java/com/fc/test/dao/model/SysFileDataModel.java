package com.fc.test.dao.model;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 文件数据外键绑定表
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_sys_file_data")
public class SysFileDataModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    /**
     * 数据id
     */
    private String dataId;

    /**
     * 文件id
     */
    private String fileId;


}
