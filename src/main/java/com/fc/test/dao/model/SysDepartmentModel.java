package com.fc.test.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 部门表
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_sys_department")
@AllArgsConstructor
@NoArgsConstructor
public class SysDepartmentModel implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    /**
     * 父id
     */
    private String parentId;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 部门负责人
     */
    private String leader;

    /**
     * 电话
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 排序
     */
    private Integer orderNum;

    @TableField(exist = false)
    private Integer childCount;
}
