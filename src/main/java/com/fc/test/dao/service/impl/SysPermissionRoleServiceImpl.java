package com.fc.test.dao.service.impl;

import com.fc.test.dao.model.SysPermissionRoleModel;
import com.fc.test.dao.mapper.SysPermissionRoleMapper;
import com.fc.test.dao.service.SysPermissionRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限中间表 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
public class SysPermissionRoleServiceImpl extends ServiceImpl<SysPermissionRoleMapper, SysPermissionRoleModel> implements SysPermissionRoleService {

}
