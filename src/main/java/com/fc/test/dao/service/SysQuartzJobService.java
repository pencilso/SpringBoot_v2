package com.fc.test.dao.service;

import com.fc.test.common.base.BaseIService;
import com.fc.test.dao.model.SysQuartzJobModel;
import com.baomidou.mybatisplus.extension.service.IService;
import org.quartz.SchedulerException;

/**
 * <p>
 * 定时任务调度表 服务类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysQuartzJobService extends BaseIService<SysQuartzJobModel> {
    /**
     * 任务调度状态修改
     *
     * @param newJob
     * @return
     */
    boolean changeStatus(SysQuartzJobModel newJob) throws SchedulerException;

    /**
     * 暂停任务
     *
     * @param job
     * @return
     * @throws SchedulerException
     */
    boolean pauseJob(SysQuartzJobModel job) throws SchedulerException;

    /**
     * 恢复任务
     *
     * @param job
     * @return
     * @throws SchedulerException
     */
    boolean resumeJob(SysQuartzJobModel job) throws SchedulerException;

    /**
     * 立即运行任务
     *
     * @param sysQuartzJob 任务
     * @throws SchedulerException
     */
    void run(SysQuartzJobModel sysQuartzJob) throws SchedulerException;
}
