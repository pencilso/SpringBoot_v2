package com.fc.test.dao.service;

import com.fc.test.dao.model.TestModel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 测试表 服务类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface TestService extends IService<TestModel> {

}
