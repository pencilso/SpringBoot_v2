package com.fc.test.dao.service;

import com.fc.test.dao.model.SysPermissionRoleModel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限中间表 服务类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysPermissionRoleService extends IService<SysPermissionRoleModel> {

}
