package com.fc.test.dao.service.impl;

import com.fc.test.common.quartz.QuartzSchedulerUtil;
import com.fc.test.common.quartz.ScheduleConstants;
import com.fc.test.dao.model.SysQuartzJobModel;
import com.fc.test.dao.mapper.SysQuartzJobMapper;
import com.fc.test.dao.service.SysQuartzJobService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 定时任务调度表 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
public class SysQuartzJobServiceImpl extends ServiceImpl<SysQuartzJobMapper, SysQuartzJobModel> implements SysQuartzJobService {
    @Autowired
    private QuartzSchedulerUtil scheduler;

    @Transactional
    @Override
    public boolean changeStatus(SysQuartzJobModel newJob) throws SchedulerException {
        Integer status = newJob.getStatus();
        if (ScheduleConstants.Status.NORMAL.getValue().equals(status)) {
            if (resumeJob(newJob)) {
                throw new RuntimeException("修改任务调度状态失败！");
            }
        } else if (ScheduleConstants.Status.PAUSE.getValue().equals(status)) {
            if (pauseJob(newJob)) {
                throw new RuntimeException("修改任务调度状态失败！");
            }
        }
        return true;
    }

    /**
     * 暂停任务
     *
     * @param job 调度信息
     */
    @Override
    public boolean pauseJob(SysQuartzJobModel job) throws SchedulerException {
        job.setStatus(ScheduleConstants.Status.PAUSE.getValue());
        //job.setUpdateBy(ShiroUtils.getLoginName());
        if (updateById(job)) {
            scheduler.pauseJob(job);
            return true;
        } else {
            return false;
        }
    }

    /**
     * 恢复任务
     *
     * @param job 调度信息
     */
    @Override
    public boolean resumeJob(SysQuartzJobModel job) throws SchedulerException {
        job.setStatus(ScheduleConstants.Status.NORMAL.getValue());
        if (updateById(job)) {
            scheduler.resumeJob(job);
            return true;
        }
        return false;
    }

    @Override
    public void run(SysQuartzJobModel sysQuartzJob) throws SchedulerException {
        scheduler.run(sysQuartzJob);
    }
}
