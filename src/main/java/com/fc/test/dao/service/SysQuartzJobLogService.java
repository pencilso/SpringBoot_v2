package com.fc.test.dao.service;

import com.fc.test.common.base.BaseIService;
import com.fc.test.dao.model.SysQuartzJobLogModel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 定时任务调度日志表 服务类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysQuartzJobLogService extends BaseIService<SysQuartzJobLogModel> {

}
