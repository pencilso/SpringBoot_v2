package com.fc.test.dao.service;

import com.fc.test.common.base.BaseIService;
import com.fc.test.dao.model.SysDictDataModel;

/**
 * <p>
 * 字典数据表 服务类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysDictDataService extends BaseIService<SysDictDataModel> {
}
