package com.fc.test.dao.service;

import com.fc.test.common.base.BaseIService;
import com.fc.test.dao.model.SysDepartmentModel;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fc.test.model.custom.BootstrapTree;

/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysDepartmentService extends BaseIService<SysDepartmentModel> {
    /**
     * 获取转换成bootstarp的权限数据
     * @return
     */
    BootstrapTree getbooBootstrapTreePerm();

}
