package com.fc.test.dao.service;

import com.fc.test.common.base.BaseIService;
import com.fc.test.dao.model.SysAreaModel;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 地区设置 服务类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysAreaService extends BaseIService<SysAreaModel> {


    /**
     * 根据城市编码查询
     * @param cityCode
     * @return
     */
    List<SysAreaModel> getByCityCode(String cityCode);
}
