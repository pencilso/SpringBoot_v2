package com.fc.test.dao.service;

import com.fc.test.dao.model.SysNoticeUserModel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 公告_用户外键 服务类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysNoticeUserService extends IService<SysNoticeUserModel> {

}
