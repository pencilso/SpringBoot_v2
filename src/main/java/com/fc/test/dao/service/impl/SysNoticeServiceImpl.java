package com.fc.test.dao.service.impl;

import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.test.common.base.PageInfo;
import com.fc.test.dao.mapper.SysNoticeMapper;
import com.fc.test.dao.model.SysNoticeModel;
import com.fc.test.dao.model.SysNoticeUserModel;
import com.fc.test.dao.model.SysUserModel;
import com.fc.test.dao.service.SysNoticeService;
import com.fc.test.dao.service.SysNoticeUserService;
import com.fc.test.model.custom.Tablepar;
import com.fc.test.shiro.util.ShiroUtils;
import com.fc.test.util.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 公告 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class SysNoticeServiceImpl extends ServiceImpl<SysNoticeMapper, SysNoticeModel> implements SysNoticeService {
    private final SysNoticeUserService sysNoticeUserService;


    @Override
    public List<SysNoticeModel> getuserNoticeNotRead(SysUserModel user, int state) {
        LambdaQueryChainWrapper<SysNoticeUserModel> lambdaQueryChainWrapper = sysNoticeUserService.lambdaQuery().eq(SysNoticeUserModel::getUserId, user.getId());
        //阅读状态
        if (-1 != state) lambdaQueryChainWrapper.eq(SysNoticeUserModel::getState, state);
        //查询列表
        List<SysNoticeUserModel> sysNoticeUserModelList = lambdaQueryChainWrapper.list();
        //查询到的数据为空  直接返回空集合
        if (sysNoticeUserModelList.isEmpty()) return Collections.emptyList();

        Set<String> noticeIds = sysNoticeUserModelList.stream().map(SysNoticeUserModel::getNoticeId).collect(Collectors.toSet());
        return lambdaQuery().in(SysNoticeModel::getId, noticeIds).list();
    }

    @Override
    public PageInfo<SysNoticeModel> listTablepar(Tablepar tablepar, String searchText) {
        LambdaQueryChainWrapper<SysNoticeModel> lambdaQuery = lambdaQuery().orderByAsc(SysNoticeModel::getId);
        if (StringUtils.isNotEmpty(searchText)) {
            lambdaQuery.like(SysNoticeModel::getTitle, searchText);
        }
        Page<SysNoticeModel> page = page(new Page<>(tablepar.getPageNum(), tablepar.getPageSize()), lambdaQuery);
        return iPageToPageInfo(page);
    }

    @Override
    public PageInfo<SysNoticeModel> listTablepar(SysUserModel sysUserModel, Tablepar tablepar, String name) {
        //查询未阅读的公告用户外键
        List<SysNoticeUserModel> sysNoticeUserModels = sysNoticeUserService.lambdaQuery().eq(SysNoticeUserModel::getUserId, sysUserModel.getId()).list();
        if (sysNoticeUserModels.isEmpty()) {
            return new PageInfo(Collections.emptyList());
        }

        Set<String> noticeIds = sysNoticeUserModels.stream().map(SysNoticeUserModel::getNoticeId).collect(Collectors.toSet());

        LambdaQueryChainWrapper<SysNoticeModel> lambdaQuery = lambdaQuery().orderByAsc(SysNoticeModel::getId).in(SysNoticeModel::getId, noticeIds);
        if (StringUtils.isNotBlank(name)) {
            lambdaQuery.like(SysNoticeModel::getTitle, name);
        }
        //分页查询
        Page<SysNoticeModel> sysNoticeModelPage = page(new Page<>(tablepar.getPageNum(), tablepar.getPageSize()), lambdaQuery);
        return iPageToPageInfo(sysNoticeModelPage);
    }

    @Override
    public int checkNameUnique(SysNoticeModel sysNotice) {
        return lambdaQuery().eq(SysNoticeModel::getTitle, sysNotice.getTitle()).list().size();
    }

    @Override
    public void editUserState(String noticeid) {
        SysNoticeUserModel sysNoticeUserModel = new SysNoticeUserModel();
        sysNoticeUserModel.setState(1);
        //更新
        sysNoticeUserService.lambdaUpdate()
                .eq(SysNoticeUserModel::getNoticeId, noticeid)
                .eq(SysNoticeUserModel::getUserId, ShiroUtils.getUserId())
                .update(sysNoticeUserModel);
    }
}
