package com.fc.test.dao.service;

import com.fc.test.common.base.BaseIService;
import com.fc.test.common.base.PageInfo;
import com.fc.test.dao.model.SysNoticeModel;
import com.fc.test.dao.model.SysUserModel;
import com.fc.test.model.custom.Tablepar;

import java.util.List;

/**
 * <p>
 * 公告 服务类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysNoticeService extends BaseIService<SysNoticeModel> {

    /**
     * 获取用户未阅读公告
     *
     * @param user
     * @param state 阅读状态  0未阅读 1 阅读  -1全部
     * @return
     */
    List<SysNoticeModel> getuserNoticeNotRead(SysUserModel user, int state);

    /**
     * 分页查询
     *
     * @param tablepar
     * @param searchText
     * @return
     */
    PageInfo<SysNoticeModel> listTablepar(Tablepar tablepar, String searchText);

    /**
     * 对应用户的所有公告信息
     *
     * @param sysUserModel
     * @param tablepar
     * @param name
     * @return
     */
    PageInfo<SysNoticeModel> listTablepar(SysUserModel sysUserModel, Tablepar tablepar, String name);

    /**
     * 检查name
     * @param sysNotice
     * @return
     */
    int checkNameUnique(SysNoticeModel sysNotice);

    /**
     * 根据公告id把当前用户的公告置为以查看
     * @param id
     */
    void editUserState(String noticeid);
}
