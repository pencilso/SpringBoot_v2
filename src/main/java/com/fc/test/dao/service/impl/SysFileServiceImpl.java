package com.fc.test.dao.service.impl;

import cn.hutool.core.io.FileUtil;
import com.fc.test.common.conf.V2Config;
import com.fc.test.common.support.Convert;
import com.fc.test.dao.model.SysDatasModel;
import com.fc.test.dao.model.SysFileDataModel;
import com.fc.test.dao.model.SysFileModel;
import com.fc.test.dao.mapper.SysFileMapper;
import com.fc.test.dao.service.SysDatasService;
import com.fc.test.dao.service.SysFileDataService;
import com.fc.test.dao.service.SysFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.test.shiro.util.ShiroUtils;
import com.fc.test.util.SnowflakeIdWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ClassUtils;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 文件信息表 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SysFileServiceImpl extends ServiceImpl<SysFileMapper, SysFileModel> implements SysFileService {
    private final SysFileDataService sysFileDataService;
    private final SysDatasService sysDatasService;

    @Transactional
    @Override
    public boolean insertSelective(SysFileModel record, String dataId) {
        //插入创建人id
        record.setCreateUserId(ShiroUtils.getUserId());
        //插入创建人name
        record.setCreateUserName(ShiroUtils.getLoginName());
        //插入创建时间
        record.setCreateTime(new Date());
        //添加雪花主键id
        record.setId(SnowflakeIdWorker.getUUID());
        //插入关联表
        SysFileDataModel tsysFileData = new SysFileDataModel();
        tsysFileData.setId(SnowflakeIdWorker.getUUID());
        tsysFileData.setFileId(record.getId());
        tsysFileData.setDataId(dataId);
        if (sysFileDataService.save(tsysFileData)) {
            throw new RuntimeException("保存文件数据失败");
        }
        if (save(record)) {
            throw new RuntimeException("保存文件失败");
        }
        return true;
    }

    @Override
    public boolean deleteBydataFile(String ids) {
        List<String> lista = Convert.toListStrArray(ids);
        List<SysDatasModel> sysDatasModels = sysDatasService.lambdaQuery().in(SysDatasModel::getId, lista).list();
        //删除本地文件
        sysDatasModels.forEach(it -> {
            deletefile(it.getFilePath());
            sysDatasService.removeById(it.getId());
        });
        return true;
    }

    @Override
    public boolean updateByPrimaryKey(SysFileModel record, String dataId) {
        //获取旧数据
        SysFileModel old_data = getById(record.getId());

        //删除绑定数据
        sysFileDataService.lambdaUpdate()
                .eq(SysFileDataModel::getFileId, record.getId())
                .remove();


        //插入关联表
        SysFileDataModel tsysFileData = new SysFileDataModel();
        tsysFileData.setId(SnowflakeIdWorker.getUUID());
        tsysFileData.setFileId(record.getId());
        tsysFileData.setDataId(dataId);
        if (!sysFileDataService.save(tsysFileData)) {
            throw new RuntimeException("插入关联信息失败！");
        }

        //修改旧数据
        //插入修改人id
        old_data.setUpdateUserId(ShiroUtils.getUserId());
        //插入修改人name
        old_data.setUpdateUserName(ShiroUtils.getLoginName());
        //插入修改时间
        old_data.setUpdateTime(new Date());
        if (!updateById(old_data)) {
            throw new RuntimeException("修改数据失败");
        }
        return true;
    }

    /**
     * 删除本地文件方法
     */
    public void deletefile(String filePath) {
        if ("Y".equals(V2Config.getIsstatic())) {
            String url = ClassUtils.getDefaultClassLoader().getResource("").getPath() + filePath;
            FileUtil.del(url);
        } else {
            FileUtil.del(filePath);
        }
    }
}
