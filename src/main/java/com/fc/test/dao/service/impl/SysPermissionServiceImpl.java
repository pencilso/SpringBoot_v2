package com.fc.test.dao.service.impl;

import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.fc.test.common.support.Convert;
import com.fc.test.dao.model.SysPermissionModel;
import com.fc.test.dao.mapper.SysPermissionMapper;
import com.fc.test.dao.model.SysPermissionRoleModel;
import com.fc.test.dao.service.SysPermissionRoleService;
import com.fc.test.dao.service.SysPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.test.model.custom.BootstrapTree;
import com.fc.test.util.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermissionModel> implements SysPermissionService {
    private final SysPermissionRoleService sysPermissionRoleService;

    @Override
    public BootstrapTree getbooBootstrapTreePerm(String userId) {
        List<BootstrapTree> treeList = new ArrayList<BootstrapTree>();
        List<SysPermissionModel> menuList = getall(userId);
        treeList = getbooBootstrapTreePerm(menuList, "0");
        if (treeList != null && treeList.size() == 1) {
            return treeList.get(0);
        }
        return new BootstrapTree("菜单", "fa fa-home", "", "-1", "###", 0, treeList, "", 0);
    }

    /**
     * 获取树
     *
     * @param menuList
     * @param parentId
     * @return
     */
    private static List<BootstrapTree> getbooBootstrapTreePerm(List<SysPermissionModel> menuList, String parentId) {
        List<BootstrapTree> treeList = new ArrayList<>();
        List<BootstrapTree> childList = null;
        for (SysPermissionModel p : menuList) {
            p.setPid(p.getPid() == null || p.getPid().trim().equals("") ? "0" : p.getPid());
            if (p.getPid().trim().equals(parentId)) {
                if (p.getChildCount() != null && p.getChildCount() > 0) {
                    childList = getbooBootstrapTreePerm(menuList, String.valueOf(p.getId()));
                }
                BootstrapTree bootstrapTree = new BootstrapTree(p.getName(), p.getIcon(), "", String.valueOf(p.getId()), p.getUrl(), p.getIsBlank(), childList, p.getPerms(), p.getVisible());
                treeList.add(bootstrapTree);
                childList = null;
            }
        }
        return treeList.size() > 0 ? treeList : null;
    }

    @Override
    public List<SysPermissionModel> getall(String userid) {
        if (StringUtils.isEmpty(userid)) {
            return lambdaQuery().orderByAsc(SysPermissionModel::getOrderNum).list();
        }
        return findByAdminUserId(userid);
    }


    @Override
    public List<SysPermissionModel> findByAdminUserId(String userid) {
        return baseMapper.findByAdminUserId(userid);
    }

    @Override
    public List<SysPermissionModel> list2(String searchText) {
        LambdaQueryChainWrapper<SysPermissionModel> lambdaQueryChainWrapper = lambdaQuery().orderByAsc(SysPermissionModel::getOrderNum);
        if (StringUtils.isNotBlank(searchText)) {
            lambdaQueryChainWrapper.like(SysPermissionModel::getName, searchText);
        }
        return lambdaQueryChainWrapper.list();
    }

    @Override
    public boolean queryLikePerms(String perms) {
        List<SysPermissionModel> list = lambdaQuery().like(SysPermissionModel::getPerms, "gen:" + perms).list();
        return !list.isEmpty();
    }

    @Override
    public List<SysPermissionModel> queryRoleId(String rolid) {
        return baseMapper.queryRoleId(rolid);
    }

    @Override
    public int deleteByPrimaryKey(String ids) {
        //转成集合
        List<String> lista = Convert.toListStrArray(ids);

        List<SysPermissionRoleModel> sysPermissionRoleModels = sysPermissionRoleService.lambdaQuery().in(SysPermissionRoleModel::getPermissionId, lista).list();
        //判断角色是否删除去除
        if (sysPermissionRoleModels.size() > 0) {//有角色外键
            return -2;
        }

        //判断是否有子集
        List<SysPermissionModel> sysPermissionModels = lambdaQuery().in(SysPermissionModel::getId, lista).list();
        boolean flag = !sysPermissionModels.isEmpty();
        if (flag) {
            //有子集 无法删除
            return -1;
        } else {
            //删除操作
            boolean remove = lambdaUpdate().in(SysPermissionModel::getId, lista).remove();
            return remove ? 1 : 0;
        }
    }

    @Override
    public BootstrapTree getCheckPrem(String roleId) {
        Map<String, Object> map = new HashMap<String, Object>();
        //设置选中
        map.put("checked", true);
        //设置展开
        //map.put("expanded", true);
        // 获取角色的权限
        List<SysPermissionModel> myTsysPermissions = queryRoleId(roleId);
        // 获取所有的权限
        BootstrapTree sysPermissions = getbooBootstrapTreePerm(null);
        iterationCheckPre(sysPermissions, myTsysPermissions, map);
        return sysPermissions;
    }
    /**
     * 循环迭代获取所有权限
     * @param pboostrapTree
     * @param myTsysPermissions
     * @param map
     */
    public void iterationCheckPre(BootstrapTree pboostrapTree, List<SysPermissionModel> myTsysPermissions, Map<String, Object> map) {
        if(null!=pboostrapTree) {
            if (ifpermissions(myTsysPermissions, pboostrapTree)) {
                pboostrapTree.setState(map);
            }
            List<BootstrapTree> bootstrapTreeList = pboostrapTree.getNodes();
            if(null!=bootstrapTreeList&&!bootstrapTreeList.isEmpty()) {
                for(BootstrapTree bootstrapTree : bootstrapTreeList) {
                    if (ifpermissions(myTsysPermissions, bootstrapTree)) {// 菜单栏设置
                        bootstrapTree.setState(map);
                    }
                    //检查子节点
                    iterationCheckPre(bootstrapTree, myTsysPermissions, map);
                }
            }
        }
    }
    /**
     * 判断权限是否有权限
     * @param myTsysPermissions
     * @param sysPermission
     */
    public Boolean ifpermissions(List<SysPermissionModel> myTsysPermissions, BootstrapTree sysPermission){
        for (SysPermissionModel mytsysPermission : myTsysPermissions) {
            if(sysPermission.getId().equals(mytsysPermission.getId())){
                return true;
            }
        }
        return false;
    }

}
