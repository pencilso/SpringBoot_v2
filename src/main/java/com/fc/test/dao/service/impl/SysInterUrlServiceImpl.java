package com.fc.test.dao.service.impl;

import com.fc.test.dao.model.SysInterUrlModel;
import com.fc.test.dao.mapper.SysInterUrlMapper;
import com.fc.test.dao.service.SysInterUrlService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 拦截url表 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
public class SysInterUrlServiceImpl extends ServiceImpl<SysInterUrlMapper, SysInterUrlModel> implements SysInterUrlService {

}
