package com.fc.test.dao.service;

import com.fc.test.dao.model.SysDatasModel;
import com.fc.test.dao.model.SysFileDataModel;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 文件数据外键绑定表 服务类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysFileDataService extends IService<SysFileDataModel> {

    /**
     * 根据文件列表id查询出对应的文件信息,已经支持多个图片获取
     *
     * @param fileid
     * @return
     */
    List<SysDatasModel> queryfileID(String fileid);
}
