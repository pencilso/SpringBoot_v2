package com.fc.test.dao.service;

import com.fc.test.common.base.BaseIService;
import com.fc.test.dao.model.SysDatasModel;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * <p>
 * 文件表存储表 服务类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysDatasService extends BaseIService<SysDatasModel> {
    /**
     * 文件上传文件存储到文件表
     * @param file
     * @return
     */
    String insertSelective(MultipartFile file) throws IOException;
}
