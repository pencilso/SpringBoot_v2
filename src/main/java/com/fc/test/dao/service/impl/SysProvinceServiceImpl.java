package com.fc.test.dao.service.impl;

import com.fc.test.dao.model.SysProvinceModel;
import com.fc.test.dao.mapper.SysProvinceMapper;
import com.fc.test.dao.service.SysProvinceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 省份表 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
public class SysProvinceServiceImpl extends ServiceImpl<SysProvinceMapper, SysProvinceModel> implements SysProvinceService {

}
