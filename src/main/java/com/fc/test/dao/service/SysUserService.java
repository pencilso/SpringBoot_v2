package com.fc.test.dao.service;

import com.fc.test.common.base.BaseIService;
import com.fc.test.dao.model.SysUserModel;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fc.test.model.custom.RoleVo;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysUserService extends BaseIService<SysUserModel> {
    /**
     * 根据username 查询
     *
     * @param username
     * @return
     */
    SysUserModel queryUserName(String username);

    /**
     * 添加用户跟角色信息
     *
     * @param user
     * @param roles
     * @return
     */
    boolean insertUserRoles(SysUserModel user, List<String> roles);

    /**
     * 获取所有权限 并且增加是否有权限字段
     *
     * @param id
     * @return
     */
    List<RoleVo> getUserIsRole(String id);

    /**
     * 修改用户信息以及角色信息
     * @param tsysUser
     * @param roles
     * @return
     */
    boolean updateUserRoles(SysUserModel tsysUser, List<String> roles);
}
