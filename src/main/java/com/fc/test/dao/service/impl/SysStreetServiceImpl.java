package com.fc.test.dao.service.impl;

import com.fc.test.dao.model.SysStreetModel;
import com.fc.test.dao.mapper.SysStreetMapper;
import com.fc.test.dao.service.SysStreetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 街道设置 服务实现类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
@Service
public class SysStreetServiceImpl extends ServiceImpl<SysStreetMapper, SysStreetModel> implements SysStreetService {

}
