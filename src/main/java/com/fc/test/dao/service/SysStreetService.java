package com.fc.test.dao.service;

import com.fc.test.common.base.BaseIService;
import com.fc.test.dao.model.SysStreetModel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 街道设置 服务类
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysStreetService extends BaseIService<SysStreetModel> {

}
