package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysFileDataModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 文件数据外键绑定表 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysFileDataMapper extends BaseMapper<SysFileDataModel> {

}
