package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysPositionModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 岗位表 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysPositionMapper extends BaseMapper<SysPositionModel> {

}
