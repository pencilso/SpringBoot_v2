package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysInterUrlModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 拦截url表 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysInterUrlMapper extends BaseMapper<SysInterUrlModel> {

}
