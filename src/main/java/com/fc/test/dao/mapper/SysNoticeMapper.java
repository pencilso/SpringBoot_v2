package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysNoticeModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 公告 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysNoticeMapper extends BaseMapper<SysNoticeModel> {

}
