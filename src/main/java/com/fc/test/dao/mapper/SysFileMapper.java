package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysFileModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 文件信息表 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysFileMapper extends BaseMapper<SysFileModel> {

}
