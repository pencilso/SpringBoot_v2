package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysCityModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 城市设置 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysCityMapper extends BaseMapper<SysCityModel> {

}
