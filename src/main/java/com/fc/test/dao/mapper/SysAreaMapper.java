package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysAreaModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 地区设置 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysAreaMapper extends BaseMapper<SysAreaModel> {

}
