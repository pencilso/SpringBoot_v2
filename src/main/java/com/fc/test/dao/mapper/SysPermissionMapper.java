package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysPermissionModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysPermissionMapper extends BaseMapper<SysPermissionModel> {
    /**
     * 根据用户id查询出用户的所有权限
     *
     * @param userId
     * @return
     */
    List<SysPermissionModel> findByAdminUserId(@Param("userId") String userId);

    /**
     * 根据角色id查询
     * @param rolid
     * @return
     */
    List<SysPermissionModel> queryRoleId(@Param("roleid") String rolid);
}
