package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysEmailModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 电子邮件 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysEmailMapper extends BaseMapper<SysEmailModel> {

}
