package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysNoticeUserModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 公告_用户外键 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysNoticeUserMapper extends BaseMapper<SysNoticeUserModel> {

}
