package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysDictTypeModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字典类型表 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysDictTypeMapper extends BaseMapper<SysDictTypeModel> {

}
