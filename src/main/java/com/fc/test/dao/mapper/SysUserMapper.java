package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysUserModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysUserMapper extends BaseMapper<SysUserModel> {

}
