package com.fc.test.dao.mapper;

import com.fc.test.dao.model.SysProvinceModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 省份表 Mapper 接口
 * </p>
 *
 * @author pencilso
 * @since 2020-04-22
 */
public interface SysProvinceMapper extends BaseMapper<SysProvinceModel> {

}
