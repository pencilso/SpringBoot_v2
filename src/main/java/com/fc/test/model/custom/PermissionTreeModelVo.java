package com.fc.test.model.custom;

import com.fc.test.dao.model.SysPermissionModel;

import java.util.List;

/**
 * 权限树
 * @author fuce 
 * @date: 2018年9月8日 下午6:40:29
 */
public class PermissionTreeModelVo {
	private SysPermissionModel tsysPermission;
	
	List<PermissionTreeModelVo> childList;//子类

	public SysPermissionModel getTsysPermission() {
		return tsysPermission;
	}

	public void setTsysPermission(SysPermissionModel tsysPermission) {
		this.tsysPermission = tsysPermission;
	}

	public List<PermissionTreeModelVo> getChildList() {
		return childList;
	}

	public void setChildList(List<PermissionTreeModelVo> childList) {
		this.childList = childList;
	}

	public PermissionTreeModelVo(SysPermissionModel tsysPermission,
								 List<PermissionTreeModelVo> childList) {
		super();
		this.tsysPermission = tsysPermission;
		this.childList = childList;
	}

	public PermissionTreeModelVo() {
		super();
	}
	

	

	
	
}
